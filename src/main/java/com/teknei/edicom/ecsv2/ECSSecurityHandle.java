package com.teknei.edicom.ecsv2;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
/** CXF 2.7.12
import org.apache.ws.security.WSPasswordCallback;
*/
/** CXF 3.1.8 */
import org.apache.wss4j.common.ext.WSPasswordCallback;


/**
 * The Class ECSSecurityHandle.
 */
public class ECSSecurityHandle implements CallbackHandler {

	
    @Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {		
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof WSPasswordCallback) {
				WSPasswordCallback pwdCallback = (WSPasswordCallback) callbacks[i];				   
				EcsContextInfo eci = EcsContextInfo.getInstance();				        
				pwdCallback.setIdentifier(eci.getUser());				        
				pwdCallback.setPassword(eci.getPass());
			}
		}
		
	}
	

}
