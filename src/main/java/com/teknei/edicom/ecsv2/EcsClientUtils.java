package com.teknei.edicom.ecsv2;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.transports.http.configuration.ProxyServerType;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.log4j.Logger;
import org.apache.wss4j.common.util.DOM2Writer;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

import com.teknei.edicom.ecsv2.ECSSecurityHandle;



public class EcsClientUtils {

	private static final Logger LOG = Logger.getLogger(EcsClientUtils.class);
	
	
	public static final int       TIPEOUT_CON  = 60000;  // 1 min      
	public static final int       TIMEOUT_REC  = 300000; // 5 min

	
	public static final HostnameVerifier hv = new HostnameVerifier() {
	    public boolean verify(String urlHostName, SSLSession session) {
	        return true;
	    }
	};

	
    public static WSS4JOutInterceptor getInterceptor (String user, ECSSecurityHandle ecsSecHandle) {
        Map<String, Object> outProps = new HashMap<>();
        outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        outProps.put(WSHandlerConstants.USER, user);
        outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
        outProps.put(WSHandlerConstants.PW_CALLBACK_REF, ecsSecHandle);
        return new WSS4JOutInterceptor(outProps);
    }
	
	public static TrustManager[] getTrustManager() {
	    //Creating Trust Manager
        return new TrustManager[] {
              new X509TrustManager() {                
                  public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                      return null;                 
                  }
                  public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                      //
                  }                  
                  public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                      /**
                      System.out.println("authType is " + authType);                     
                      System.out.println("cert issuers");                                        
                      for (int i = 0; i < certs.length; i++) {                   
                          System.out.println("\t" + certs[i].getIssuerX500Principal().getName());                    
                          System.out.println("\t" + certs[i].getIssuerDN().getName());           
                      }
                      */
                  }        
              }     
        };
	}

	
	public static SSLSocketFactory getSSLSocketFactory() throws NoSuchAlgorithmException, KeyManagementException  {
	    TrustManager[] trustAllCerts = EcsClientUtils.getTrustManager();	    
	    SSLContext sc = SSLContext.getInstance("SSLv3");	    
	    sc.init(null, trustAllCerts, new java.security.SecureRandom());
	    return sc.getSocketFactory();	    
	}
	

	
	public static TLSClientParameters getTlsClientParameters() {
		TLSClientParameters tls = new TLSClientParameters();		
		TrustManager[] trustAllCerts = getTrustManager();
		tls.setDisableCNCheck(true);
		tls.setTrustManagers(trustAllCerts);
		return tls;
	}

	
	public static void setProxyParams(HTTPClientPolicy httpClientPolicy, HTTPConduit http) {
        // http
		String proxyType   		= "HTTP";
        String proxyServer 		= System.getProperty("http.proxyHost");
        int iProxyPort 			= str2intdef(System.getProperty("http.proxyPort"),	80);
        String proxyUser 		= System.getProperty("http.proxyUser");
   	    String proxyPassword 	= System.getProperty("http.proxyPassword");
        if (StringUtils.isEmpty(proxyServer)) {
        	// https
        	proxyType   		= "HTTPS";
        	proxyServer 		= System.getProperty("https.proxyHost");
        	iProxyPort 			= str2intdef(System.getProperty("https.proxyPort"),	80);
        	proxyUser 			= System.getProperty("https.proxyUser");
        	proxyPassword 		= System.getProperty("https.proxyPassword");
        	if (StringUtils.isEmpty(proxyServer)) {
        		// socks
        		proxyType   	= "SOCKS";
        		proxyServer 	= System.getProperty("socksProxyHost");
        		iProxyPort 		= str2intdef(System.getProperty("socksProxyPort"),	80);
        		proxyUser 		= System.getProperty("java.net.socks.username");
            	proxyPassword 	= System.getProperty("java.net.socks.password");
        	}
        }
        //
        if (!StringUtils.isEmpty(proxyServer)) {
        	httpClientPolicy.setProxyServer(proxyServer);
        	httpClientPolicy.setProxyServerPort(iProxyPort);
        	if (!StringUtils.isEmpty(proxyType)) {
        		if ("SOCKS".equals(proxyType)) {
        			httpClientPolicy.setProxyServerType(ProxyServerType.SOCKS);
        		} else {
        			httpClientPolicy.setProxyServerType(ProxyServerType.HTTP);
        		}
        	}
        	if (!StringUtils.isEmpty(proxyUser)) {
        		http.getProxyAuthorization().setUserName(proxyUser);
        		http.getProxyAuthorization().setPassword(proxyPassword);
        	}
        }
        //
	}
	
    public static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        docBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true); // Juan 06/05/2016 Xml vulnerate to XXE
        docBuilderFactory.setValidating(false);
        docBuilderFactory.setAttribute("http://apache.org/xml/features/continue-after-fatal-error", Boolean.TRUE); // FRAN 19/12/2007: Ignorar error de sintaxis en lectura
        docBuilderFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        docBuilder.setEntityResolver(new EntityResolver() {
            public InputSource resolveEntity(String publicId, String systemId) {
                return new InputSource(new StringReader(""));
            }
        });
        return docBuilder;
    }
	
	
	public static Document streamToDocument(InputStream is) {
	    try {
	        if (is == null) {
	            return null;
	        }
	        DocumentBuilder docBuilder = getDocumentBuilder();
	        return docBuilder.parse(is);
	    } catch (Exception E) {
	        LOG.error("", E);
	        return null;
	    }
	}

	public static byte[] documentToBytes(Document doc) {
	    try {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        OutputStreamWriter osw = new OutputStreamWriter(baos, "UTF-8");
	        DOM2Writer.serializeAsXML(doc, osw, true, false);            
	        return baos.toByteArray();
	        //
	    } catch (Exception e) {
	        LOG.error("", e); 
	        return null;            
	    }
	}

	
	public static int str2intdef(String str, int def) {
        int result = def;
        if (str == null) {
            return result;
        }
        try {
            str = str.trim();
            if (str.length() > 0) {
                result = Integer.parseInt(str.trim());
            }
        } catch (Exception E) {
            result = def;
        }
        return result;
    }
	
	public static Properties initializateCryptoProperties(String ksLoc, String ksPwd, String kwCertAlias) {
        Properties res = new Properties();
        res.setProperty("org.apache.ws.security.crypto.provider", 					"org.apache.ws.security.components.crypto.Merlin");
        res.setProperty("org.apache.ws.security.crypto.merlin.cert.provider", 		"BC");  // Juan 03/03/2011     
        res.setProperty("org.apache.ws.security.crypto.merlin.keystore.type", 		"pkcs12");
        res.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", 	ksPwd);
        res.setProperty("org.apache.ws.security.crypto.merlin.keystore.alias", 		kwCertAlias);
        res.setProperty("org.apache.ws.security.crypto.merlin.alias.password", 		ksPwd);
        res.setProperty("org.apache.ws.security.crypto.merlin.file", 				ksLoc);        
        return res;        
    }

	
	public static String findKeyAlias(String storeLocation, String storeType, String password) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore 	store = KeyStore.getInstance(storeType);					
    	return findKeyAlias(store, storeLocation, password.toCharArray());    	    		
	}

	
	public static String findKeyAlias(KeyStore store, String storeName, char[] password) throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
		InputStream is = new FileInputStream(storeName);
		try {
			store.load(is, password);
		} finally {
			is.close();
		}
		Enumeration e = store.aliases();
		String keyAlias = null;

		while (e.hasMoreElements()) {
			String alias = (String) e.nextElement();
			if (store.isKeyEntry(alias)) {
				keyAlias = alias;
			}
		}
		if (keyAlias == null) {
		    throw new IllegalArgumentException("can't find a private key in keyStore: " + storeName);		
		}
		return keyAlias;
	}

	

}
