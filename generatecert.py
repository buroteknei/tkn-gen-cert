#!/usr/bin/python
import os
from subprocess import call

print "Generating certificate"

#Parameters of the first call
wsdlUri = "https://ecsweb.sedeb2b.com:9125/EdicomCryptoServer/services/cert"
jarFile = "/home/amaro/Documentos/TKN/user_cert/tkn-gen-cert.jar"
certFile = "/home/amaro/Documentos/TKN/PSC/BURO_SSL_1.P12"
certPass = "BID"
customerName = "ANDRESFLORES"
customerId = "1231000"
customerMail = "aflores@tenkei.com"
customerUsername = "aflores1233"
customerPassword = "aflores1233"
customerOutputDir = "/home/amaro/Documentos/TKN/user_cert"

call(["java", "-jar", jarFile, 
	  wsdlUri, 
	  certFile, certPass, customerName, 
	  customerId, customerMail, customerUsername, customerPassword, customerOutputDir
	 ])

print "Cert generated"

print "Signing document"

wsdlUri2 = "https://ecsweb.sedeb2b.com:9125/EdicomCryptoServer/services/firma"
user2 = "BID170328LY0_SBDL"
pass2 = "plpn0wpcd9"
certUser2 = customerUsername
certPass2 = customerPassword
certUserString = open('/home/amaro/Documentos/TKN/user_cert/'+customerId+customerUsername+'_serial.txt', 'r').read()
userPDFUri = "/home/amaro/Documentos/TKN/user_cert/contrato.pdf"
jarFile2 = "/home/amaro/Documentos/TKN/user_cert/tkn-sign-doc.jar"

print "Retrieved cert:"+certUserString

call(["java", "-jar", jarFile2, 
	  wsdlUri2, 
	  user2, pass2, certUser2, 
	  certPass2, certUserString, userPDFUri
	 ])

print "Document signed"