package com.teknei.edicom.ecs.cert.client;

public class CertException extends Exception {
	private static final long serialVersionUID = 1L;

	public static final int ERROR_DESCONOCIDO = -1;

	public static final int ERROR_LOG4J = 1;

	public static final int ERROR_PROPERTIES = 2;
	public static final int ERROR_PARAMETROS = 3;
	public static final int ERROR_PROCESO = 4;
	public static final int ERROR_DOCUMENT = 5;
	protected int codException;
	protected String textException;

	public CertException() {
		setCodException(-1);
		setTextException("ERROR");
	}

	public CertException(String message) {
		super(message);
		setCodException(-1);
		setTextException(message);
	}

	public CertException(int codException, String message) {
		super(message);
		setCodException(codException);
		setTextException(message);
	}

	public int getCodException() {
		return this.codException;
	}

	private void setCodException(int codException) {
		this.codException = codException;
	}

	public String getTextException() {
		return this.textException;
	}

	private void setTextException(String textException) {
		this.textException = textException;
	}

	public String getTextCode() {
		return getTextCode(getCodException());
	}

	public static String getTextCode(int codeException) {
		String s = "ERROR";
		switch (codeException) {
		case -1:
			s = "Unknown error.";
			break;
		case 1:
			s = "It was not possible to initialize the client log system.";
			break;
		case 2:
			s = "Could not load client property file.";
			break;
		case 3:
			s = "Error in the parameters needed to invoke the service.";
			break;
		case 4:
			s = "There was an error invoking the service.";
			break;
		case 5:
			s = "There was an error generating the document.";
			break;
		}

		return s;
	}
}
