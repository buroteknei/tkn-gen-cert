package com.teknei.edicom.ecsv2;



/**
 * The Class EcsContextHolder.
 */
public class EcsContextHolder {
	
	private static final ThreadLocal<EcsContextInfo> CONTEXTHOLDER = new ThreadLocal<>();

	
	public static void setContextInfo(EcsContextInfo contextInfo) {
		CONTEXTHOLDER.set(contextInfo);
	}

	public static EcsContextInfo getContextInfo() {
		return CONTEXTHOLDER.get();
	}

	public static void clearContextInfo() {
		CONTEXTHOLDER.remove();
	}

}
