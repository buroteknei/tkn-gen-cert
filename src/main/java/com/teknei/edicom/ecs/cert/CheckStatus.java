package com.teknei.edicom.ecs.cert;

import com.sanityinc.jargs.CmdLineParser;
import com.teknei.edicom.ecs.cert.client.CertClient;
import com.teknei.edicom.ecs.cert.client.CertException;
import com.teknei.edicom.ecs.cert.helper.GestorLogs;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

public class CheckStatus extends CertProgram {
	CheckStatus() throws CertException {
	}

	public void showHelp() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("CheckStatus: \n");
		buffer.append("\n");
		buffer.append("How to use: CheckStatus ");
		buffer.append("{-").append('u').append(", --").append("user").append("} user").append(", ");
		buffer.append("{-").append('p').append(", --").append("pass").append("} pass").append(", ");

		buffer.append("{-").append('q').append(", --").append("checkType").append("} checkType]");

		GestorLogs.info(CheckStatus.class, "Help", buffer.toString());
	}

	void addArguments(String[] args) throws CertException {
		CmdLineParser parser = new CmdLineParser();

		CmdLineParser.Option<String> url = parser.addStringOption('l', "url");
		CmdLineParser.Option<String> pfx = parser.addStringOption('s', "pfx");
		CmdLineParser.Option<String> key = parser.addStringOption('k', "key");

		CmdLineParser.Option<String> user = parser.addStringOption('u', "user");
		CmdLineParser.Option<String> pass = parser.addStringOption('p', "pass");

		CmdLineParser.Option<String> type = parser.addStringOption('q', "checkType");
		try {
			parser.parse(args, Locale.getDefault());
		} catch (CmdLineParser.OptionException optionException) {
			String texto = "Error while processing client parameters: ";
			GestorLogs.error(CheckStatus.class, "Add Arguments", texto + optionException.toString(), optionException);
			showHelp();
		}

		String urlValue = (String) parser.getOptionValue(url);
		String pfxValue = (String) parser.getOptionValue(pfx);
		String keyValue = (String) parser.getOptionValue(key);

		String userValue = (String) parser.getOptionValue(user);
		String passValue = (String) parser.getOptionValue(pass);

		String typeValue = (String) parser.getOptionValue(type);

		insertaValorProperties("url", urlValue);
		insertaValorProperties("pfx", pfxValue);
		insertaValorProperties("key", keyValue);

		insertaValorProperties("user", userValue);
		insertaValorProperties("pass", passValue);

		insertaValorProperties("checkType", typeValue);
	}

	boolean validaDatos() throws CertException {
		boolean validateIn = false;
		try {
			String type = getProperties().getProperty("checkType");
			if (StringUtils.isNumeric(type)) {
				validateIn = true;
			}
		} catch (Exception exception) {
			GestorLogs.error(CheckStatus.class, "Exception", exception.getMessage(), exception);
		}
		return validateIn;
	}

	protected boolean checkStatus() throws CertException {
		boolean status = false;
		GestorLogs.info(CheckStatus.class, "CHECK STATUS", "start");

		try {
			CertClient client = new CertClient(getProperties().getProperty("url"),
					getProperties().getProperty("user"), getProperties().getProperty("pass"));

			int type = Integer.parseInt(getProperties().getProperty("checkType"));

			status = client.checkStatus(type);
		} catch (Exception exception) {
			GestorLogs.error(CheckStatus.class, "Client Exception", "The client could not be connected", exception);
		}
		return status;
	}

	public void checkStatusController(String[] args) throws CertException {
		try {
			if ((args.length != 0) && ((args[0].endsWith("help")) || (args[0].endsWith(String.valueOf('h'))))) {
				showHelp();
				return;
			}
			addArguments(args);
			GestorLogs.info(CheckStatus.class, "main", "Parametros: " + getProperties().toString());
			if ((verifyProperties()) && (validaDatos())) {
				boolean status = checkStatus();
				GestorLogs.info(CheckStatus.class, "CheckStatus", "Respuesta: " + status);
			} else {
				GestorLogs.error(CheckStatus.class, "CheckStatus", CertException.getTextCode(5));
			}
			GestorLogs.info(CheckStatus.class, "main", "Terminated!");
		} catch (Exception ex) {
			GestorLogs.error(CheckStatus.class, "main", ex.getMessage(), ex);
		}
	}
}
