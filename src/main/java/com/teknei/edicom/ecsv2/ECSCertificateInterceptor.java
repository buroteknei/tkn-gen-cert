package com.teknei.edicom.ecsv2;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.xml.crypto.dsig.Reference;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.log4j.Logger;
import org.apache.wss4j.common.WSEncryptionPart;
import org.apache.wss4j.common.crypto.Crypto;
import org.apache.wss4j.common.crypto.CryptoFactory;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.message.WSSecHeader;
import org.apache.wss4j.dom.message.WSSecSignature;
import org.w3c.dom.Document;

import com.teknei.edicom.ecsv2.EcsClientUtils;

public class ECSCertificateInterceptor extends AbstractPhaseInterceptor<Message> {

	private static final Logger LOG = Logger.getLogger(EcsClientUtils.class);
	
	private static final String         UTF_8           = "UTF-8";
	private static final String         BODY            = "Body";
	private static final String			SOAP_ENV	    = "http://schemas.xmlsoap.org/soap/envelope/";
	private static final String 		KEYSTORE_TYPE 	= "pkcs12";
	

	
	public ECSCertificateInterceptor () {
		super(Phase.PRE_STREAM); 
	}
	
	@Override
	public void handleMessage(Message message) {		
        try {
        	OutputStream os = message.getContent(OutputStream.class);
            CachedStream cs = new CachedStream();
            message.setContent(OutputStream.class, cs);
            message.getInterceptorChain().doIntercept(message);
            //
            // Leemos el soapenvelope del stream de salida
            cs.flush();
            IOUtils.closeQuietly(cs);
            CachedOutputStream csnew = (CachedOutputStream) message.getContent(OutputStream.class);
            csnew.flush();
            String currentEnvelopeMessage = IOUtils.toString(csnew.getInputStream(), UTF_8);
            //INCLUIR TEST - INICIO
            //Se marca como TEST si está marcado en el contexto
            EcsContextInfo eci = EcsContextInfo.getInstance();
            if (eci.isTest())
            	currentEnvelopeMessage = currentEnvelopeMessage.replace("</certParams>", "</certParams><test>true</test>");
            //INCLUIR TEST - FIN
            IOUtils.closeQuietly(csnew);
            //
            // Creamos el BinarySecurityToken
            String currentEnvelopeMessageWithSecurityToken = createBinarySecurityToken(currentEnvelopeMessage);
            //
            // Escribimos la nueva petici�n firmada en el stream de salida
            InputStream replaceInStream = IOUtils.toInputStream(currentEnvelopeMessageWithSecurityToken, UTF_8);
            System.out.println("Steam:"+currentEnvelopeMessage);
            System.out.println("-----------------------------");
            System.out.println("Steam:"+currentEnvelopeMessageWithSecurityToken);
            IOUtils.copy(replaceInStream, os);
            replaceInStream.close();
            IOUtils.closeQuietly(replaceInStream);
            os.flush();
            message.setContent(OutputStream.class, os);
            IOUtils.closeQuietly(os);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
	}
	
	

    private String createBinarySecurityToken(String str) {
        try {
            // Obtenci�n del documento XML que representa la petici�n SOAP        	
            Document  soapEnvelopeRequest = EcsClientUtils.streamToDocument(new ByteArrayInputStream(str.getBytes()));
            //            
            WSSecHeader wsSecHeader = new WSSecHeader(soapEnvelopeRequest);
            wsSecHeader.setMustUnderstand(false);
            wsSecHeader.insertSecurityHeader();
            //
            EcsContextInfo eci = EcsContextInfo.getInstance();
            String keystoreCertAlias   = EcsClientUtils.findKeyAlias(eci.getKeystoreLocation(), KEYSTORE_TYPE, eci.getKeystorePassword());
            Properties cryptoProperties = EcsClientUtils.initializateCryptoProperties(eci.getKeystoreLocation(), eci.getKeystorePassword(), keystoreCertAlias);
            Crypto crypto = CryptoFactory.getInstance(cryptoProperties);
            //
            WSSecSignature wsSecSignature = new WSSecSignature();
            //wsSecSignature.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);            
            wsSecSignature.setKeyIdentifierType(WSConstants.X509_KEY_IDENTIFIER);
            wsSecSignature.setUserInfo(keystoreCertAlias, eci.getKeystorePassword());            
            wsSecSignature.prepare(soapEnvelopeRequest, crypto, wsSecHeader);
            
            //
            Vector<WSEncryptionPart> wsEncryptionParts = new Vector<WSEncryptionPart>();
            wsEncryptionParts.add(new WSEncryptionPart(BODY, SOAP_ENV, null));
            List<Reference> refToSign = wsSecSignature.addReferencesToSign(wsEncryptionParts, wsSecHeader);
            //
            wsSecSignature.computeSignature(refToSign);
            wsSecSignature.prependBSTElementToHeader(wsSecHeader);
            //
            byte[] docbytes = EcsClientUtils.documentToBytes(soapEnvelopeRequest);
			//
			return new String(docbytes, UTF_8);                                  
            //
        } catch (WSSecurityException | IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
        	LOG.error(e);
            return null; 
        }
    }        

    
    private class CachedStream extends CachedOutputStream {
        public CachedStream() {
            super();
        }
        @Override
        protected void doFlush() throws IOException {
            currentStream.flush();
        }
        @Override
        protected void doClose() throws IOException {
            //
        }
        @Override
        protected void onWrite() throws IOException {
            //
        }
    }
    


}
