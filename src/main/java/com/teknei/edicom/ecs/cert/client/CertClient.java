package com.teknei.edicom.ecs.cert.client;

import java.net.URL;
import java.util.List;

import javax.xml.ws.Service;

import com.teknei.edicom.utils.ws.client.DefaultCxfClient;

import cert.ecs.edicom.com.GenerateUnrecognizedCertificate.CertParams;
import cert.ecs.edicom.com.WebServiceCert;
import cert.ecs.edicom.com.WebServiceCertImplService;

public class CertClient extends DefaultCxfClient {
	public CertClient(String url, String user, String pass) throws Exception {
		super(url, user, pass);
	}

	public CertClient(String wsUrl, String user, String pass, byte[] wsFilePfx, String wsPfxPass) throws Exception {
		super(wsUrl, user, pass, 30, "PasswordText", wsFilePfx, wsPfxPass);
	}

	public CertClient(String wsUrl, byte[] wsFilePfx, String wsPfxPass) throws Exception {
		super(wsUrl, null, null, 30, null, wsFilePfx, wsPfxPass);
	}

	protected Service createService(URL wsUrl) throws Exception {
		return new WebServiceCertImplService(); // MCG: Lo toma de archivo local
	}

	protected Object getServicePort(Service service) throws Exception {
		WebServiceCertImplService serviceObj = (WebServiceCertImplService) service;
		return serviceObj.getWebServiceCertImplPort();
	}

	private WebServiceCert getClient() {
		return (WebServiceCert) this.client;
	}

	public boolean checkStatus(int checkType) {
		boolean data = false;
		try {
			data = getClient().checkStatus(checkType);
		} catch (Exception e) {
			this.logger.error("checkStatus", e);
		}
		return data;
	}

	public byte[] generateUnrecognizedCertificate(String certUsr, String certPwd, String restrictedLogin, CertParams certParams, List<byte[]> evidences) {
		byte[] data = null;
		try {
			data = getClient().generateUnrecognizedCertificate(certUsr, certPwd, restrictedLogin, certParams, evidences);
		} catch (Exception e) {
			this.logger.error("generateUnrecognizedCertificate", e);
		}
		return data;
	}
}
