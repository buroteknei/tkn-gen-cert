
package cert.ecs.edicom.com;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cert.ecs.edicom.com package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckStatusResponse_QNAME = new QName("http://com.edicom.ecs.cert", "checkStatusResponse");
    private final static QName _GenerateUnrecognizedCertificateResponse_QNAME = new QName("http://com.edicom.ecs.cert", "generateUnrecognizedCertificateResponse");
    private final static QName _RevokeCertificate_QNAME = new QName("http://com.edicom.ecs.cert", "revokeCertificate");
    private final static QName _CheckStatus_QNAME = new QName("http://com.edicom.ecs.cert", "checkStatus");
    private final static QName _CreateSoftcardResponse_QNAME = new QName("http://com.edicom.ecs.cert", "createSoftcardResponse");
    private final static QName _GenerateUnrecognizedCertificate_QNAME = new QName("http://com.edicom.ecs.cert", "generateUnrecognizedCertificate");
    private final static QName _RenewCertificateResponse_QNAME = new QName("http://com.edicom.ecs.cert", "renewCertificateResponse");
    private final static QName _ECSException_QNAME = new QName("http://com.edicom.ecs.cert", "ECSException");
    private final static QName _GetSoftcardResponse_QNAME = new QName("http://com.edicom.ecs.cert", "getSoftcardResponse");
    private final static QName _RenewCertificate_QNAME = new QName("http://com.edicom.ecs.cert", "renewCertificate");
    private final static QName _CreateSoftcard_QNAME = new QName("http://com.edicom.ecs.cert", "createSoftcard");
    private final static QName _RevokeCertificateResponse_QNAME = new QName("http://com.edicom.ecs.cert", "revokeCertificateResponse");
    private final static QName _GetSoftcard_QNAME = new QName("http://com.edicom.ecs.cert", "getSoftcard");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cert.ecs.edicom.com
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenerateUnrecognizedCertificate }
     * 
     */
    public GenerateUnrecognizedCertificate createGenerateUnrecognizedCertificate() {
        return new GenerateUnrecognizedCertificate();
    }

    /**
     * Create an instance of {@link GenerateUnrecognizedCertificate.CertParams }
     * 
     */
    public GenerateUnrecognizedCertificate.CertParams createGenerateUnrecognizedCertificateCertParams() {
        return new GenerateUnrecognizedCertificate.CertParams();
    }

    /**
     * Create an instance of {@link CreateSoftcard }
     * 
     */
    public CreateSoftcard createCreateSoftcard() {
        return new CreateSoftcard();
    }

    /**
     * Create an instance of {@link RevokeCertificateResponse }
     * 
     */
    public RevokeCertificateResponse createRevokeCertificateResponse() {
        return new RevokeCertificateResponse();
    }

    /**
     * Create an instance of {@link GetSoftcard }
     * 
     */
    public GetSoftcard createGetSoftcard() {
        return new GetSoftcard();
    }

    /**
     * Create an instance of {@link GetSoftcardResponse }
     * 
     */
    public GetSoftcardResponse createGetSoftcardResponse() {
        return new GetSoftcardResponse();
    }

    /**
     * Create an instance of {@link RenewCertificate }
     * 
     */
    public RenewCertificate createRenewCertificate() {
        return new RenewCertificate();
    }

    /**
     * Create an instance of {@link RevokeCertificate }
     * 
     */
    public RevokeCertificate createRevokeCertificate() {
        return new RevokeCertificate();
    }

    /**
     * Create an instance of {@link CheckStatus }
     * 
     */
    public CheckStatus createCheckStatus() {
        return new CheckStatus();
    }

    /**
     * Create an instance of {@link CreateSoftcardResponse }
     * 
     */
    public CreateSoftcardResponse createCreateSoftcardResponse() {
        return new CreateSoftcardResponse();
    }

    /**
     * Create an instance of {@link RenewCertificateResponse }
     * 
     */
    public RenewCertificateResponse createRenewCertificateResponse() {
        return new RenewCertificateResponse();
    }

    /**
     * Create an instance of {@link ECSException }
     * 
     */
    public ECSException createECSException() {
        return new ECSException();
    }

    /**
     * Create an instance of {@link CheckStatusResponse }
     * 
     */
    public CheckStatusResponse createCheckStatusResponse() {
        return new CheckStatusResponse();
    }

    /**
     * Create an instance of {@link GenerateUnrecognizedCertificateResponse }
     * 
     */
    public GenerateUnrecognizedCertificateResponse createGenerateUnrecognizedCertificateResponse() {
        return new GenerateUnrecognizedCertificateResponse();
    }

    /**
     * Create an instance of {@link GenerateUnrecognizedCertificate.CertParams.Entry }
     * 
     */
    public GenerateUnrecognizedCertificate.CertParams.Entry createGenerateUnrecognizedCertificateCertParamsEntry() {
        return new GenerateUnrecognizedCertificate.CertParams.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "checkStatusResponse")
    public JAXBElement<CheckStatusResponse> createCheckStatusResponse(CheckStatusResponse value) {
        return new JAXBElement<CheckStatusResponse>(_CheckStatusResponse_QNAME, CheckStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateUnrecognizedCertificateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "generateUnrecognizedCertificateResponse")
    public JAXBElement<GenerateUnrecognizedCertificateResponse> createGenerateUnrecognizedCertificateResponse(GenerateUnrecognizedCertificateResponse value) {
        return new JAXBElement<GenerateUnrecognizedCertificateResponse>(_GenerateUnrecognizedCertificateResponse_QNAME, GenerateUnrecognizedCertificateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevokeCertificate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "revokeCertificate")
    public JAXBElement<RevokeCertificate> createRevokeCertificate(RevokeCertificate value) {
        return new JAXBElement<RevokeCertificate>(_RevokeCertificate_QNAME, RevokeCertificate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "checkStatus")
    public JAXBElement<CheckStatus> createCheckStatus(CheckStatus value) {
        return new JAXBElement<CheckStatus>(_CheckStatus_QNAME, CheckStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSoftcardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "createSoftcardResponse")
    public JAXBElement<CreateSoftcardResponse> createCreateSoftcardResponse(CreateSoftcardResponse value) {
        return new JAXBElement<CreateSoftcardResponse>(_CreateSoftcardResponse_QNAME, CreateSoftcardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateUnrecognizedCertificate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "generateUnrecognizedCertificate")
    public JAXBElement<GenerateUnrecognizedCertificate> createGenerateUnrecognizedCertificate(GenerateUnrecognizedCertificate value) {
        return new JAXBElement<GenerateUnrecognizedCertificate>(_GenerateUnrecognizedCertificate_QNAME, GenerateUnrecognizedCertificate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenewCertificateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "renewCertificateResponse")
    public JAXBElement<RenewCertificateResponse> createRenewCertificateResponse(RenewCertificateResponse value) {
        return new JAXBElement<RenewCertificateResponse>(_RenewCertificateResponse_QNAME, RenewCertificateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ECSException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "ECSException")
    public JAXBElement<ECSException> createECSException(ECSException value) {
        return new JAXBElement<ECSException>(_ECSException_QNAME, ECSException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSoftcardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "getSoftcardResponse")
    public JAXBElement<GetSoftcardResponse> createGetSoftcardResponse(GetSoftcardResponse value) {
        return new JAXBElement<GetSoftcardResponse>(_GetSoftcardResponse_QNAME, GetSoftcardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenewCertificate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "renewCertificate")
    public JAXBElement<RenewCertificate> createRenewCertificate(RenewCertificate value) {
        return new JAXBElement<RenewCertificate>(_RenewCertificate_QNAME, RenewCertificate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSoftcard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "createSoftcard")
    public JAXBElement<CreateSoftcard> createCreateSoftcard(CreateSoftcard value) {
        return new JAXBElement<CreateSoftcard>(_CreateSoftcard_QNAME, CreateSoftcard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevokeCertificateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "revokeCertificateResponse")
    public JAXBElement<RevokeCertificateResponse> createRevokeCertificateResponse(RevokeCertificateResponse value) {
        return new JAXBElement<RevokeCertificateResponse>(_RevokeCertificateResponse_QNAME, RevokeCertificateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSoftcard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.cert", name = "getSoftcard")
    public JAXBElement<GetSoftcard> createGetSoftcard(GetSoftcard value) {
        return new JAXBElement<GetSoftcard>(_GetSoftcard_QNAME, GetSoftcard.class, null, value);
    }

}
