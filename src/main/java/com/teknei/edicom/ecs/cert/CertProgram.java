package com.teknei.edicom.ecs.cert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;

import com.teknei.edicom.ecs.cert.client.CertException;
import com.teknei.edicom.ecs.cert.helper.FileUtils;
import com.teknei.edicom.ecs.cert.helper.GestorLogs;

public abstract class CertProgram {
	public static final String PDF_EXTENSION = ".pdf";
	public static final String ASN_EXTENSION = ".asn";
	public static final String TSR_EXTENSION = ".tsr";
	public static final String ALL_ASN_FILES = "*.asn";
	public static final String ASN_STATUS = ".status";
	public static final String ALL_FILES = "*";
	public static final String BLANK = "";
	public static final String CHECK_STATUS = "CheckStatus";
	public static final String LOAD_PROPERTIES = "Load Properties";
	public static final String INITIALIZE_LOGGER = "Initialize Logger";
	public static final String LOG_PROPERTIES_FILE = "certLog4j.properties";
	public static final String CLIENT_PROPERTIES_FILE = "certClient.properties";
	public static final String PROPERTIES_URL = "url";
	public static final char PROPERTIES_SHORT_URL = 'l';
	public static final String PROPERTIES_PFX = "pfx";
	public static final char PROPERTIES_SHORT_PFX = 's';
	public static final String PROPERTIES_KEY = "key";
	public static final char PROPERTIES_SHORT_KEY = 'k';
	public static final String PROPERTIES_USER = "user";
	public static final char PROPERTIES_SHORT_USER = 'u';
	public static final String PROPERTIES_PASS = "pass";
	public static final char PROPERTIES_SHORT_PASS = 'p';
	public static final String PROPERTIES_DOMAIN = "domain";
	public static final char PROPERTIES_SHORT_DOMAIN = 'd';
	public static final String PROPERTIES_GROUP = "group";
	public static final char PROPERTIES_SHORT_GROUP = 'g';
	public static final String PROPERTIES_FILE = "file";
	public static final char PROPERTIES_SHORT_FILE = 'f';
	public static final String PROPERTIES_HASH = "hash";
	public static final char PROPERTIES_SHORT_HASH = 't';
	public static final String PROPERTIES_CHECKTYPE = "checkType";
	public static final char PROPERTIES_SHORT_CHECKTYPE = 'q';
	public static final String PROPERTIES_HELP = "help";
	public static final char PROPERTIES_SHORT_HELP = 'h';
	private Properties properties;

	public CertProgram() throws CertException {
		initializeLogger();

		loadProperties();

		inicializaProperties();
	}

	protected Properties getProperties() {
		return this.properties;
	}

	private void initializeLogger() throws CertException {
		Properties logProperties = new Properties();
		try {
			File f = new File(getLogPropertiesFile());
			logProperties.load(new FileInputStream(f));
			PropertyConfigurator.configure(logProperties);
			GestorLogs.info(this, "Initialize Logger", "Logging inicializado: " + getLogPropertiesFile() + ".");
		} catch (IOException ioException) {
			String error = "initializeLogger: Error al cargar el fichero del log de la aplicacion: "
					+ getLogPropertiesFile() + ".";
			GestorLogs.info(this, "Initialize Logger", error, ioException);
			throw new CertException(1, error);
		} catch (Exception exception) {
			String error = "initializeLogger: Error al inicializar el log de la aplicacion: " + getLogPropertiesFile()
					+ ".";
			GestorLogs.info(this, "Initialize Logger", error, exception);
			throw new CertException(1, error);
		}
	}

	private void loadProperties() throws CertException {
		GestorLogs.info(this, "loadProperties", "start");
		String errorText = "";
		this.properties = new Properties();
		try {
			File f = new File(getClientPropertiesFile());
			getProperties().load(new FileInputStream(f));
		} catch (FileNotFoundException fnfException) {
			errorText = "Fichero de propiedades no encontrado " + getClientPropertiesFile() + ".";
			GestorLogs.error(this, "Load Properties", errorText, fnfException);
			throw new CertException(2, errorText);
		} catch (IOException ioException) {
			errorText = "No se puede leer el fichero de propiedades " + getClientPropertiesFile() + ".";
			GestorLogs.error(this, "Load Properties", errorText, ioException);
			throw new CertException(2, errorText);
		} catch (Exception exception) {
			errorText = "Error al leer el fichero de propiedades " + getClientPropertiesFile() + ".";
			GestorLogs.error(this, "Load Properties", errorText, exception);
			throw new CertException(2, errorText);
		}

		GestorLogs.info(this, "Load Properties", "end");
	}

	protected void insertaValorProperties(String key, String value) {
		if (value != null) {
			getProperties().setProperty(key, value);
		}
	}

	public void inicializaProperties() {
		GestorLogs.info(this, "Inicializa Properties", "start");

		String rutaAplicacion = FileUtils.preparaRutaDirectorio(System.getProperty("user.dir"));
		String in = getProperties().getProperty("in");
		if ((in == null) || (in.equals(""))) {
			getProperties().setProperty("in", rutaAplicacion);
		}
		GestorLogs.info(this, "Inicializa Properties", "end");
	}

	protected void insertaValorProperties(String key, Boolean value) {
		if (value != null) {
			insertaValorProperties(key, value.toString());
		}
	}

	protected boolean verifyProperty(String key) {
		boolean ok = getProperties().containsKey(key);
		if (ok) {
			String value = getProperties().getProperty(key);
			ok = (value != null) && (!value.equals(""));
		}
		return ok;
	}

	protected boolean verifyProperties() throws CertException {
		if (!verifyProperty("user"))
			throw new CertException(3, "Falta por insertar el usuario que accede al servicio.");
		if (!verifyProperty("pass"))
			throw new CertException(3, "Falta por insertar la clave del usuario que accede al servicio.");
		if (!verifyProperty("url"))
			throw new CertException(3, "Falta por insertar la url del servicio.");
		if (!verifyProperty("certUsr"))
			throw new CertException(3, "Falta por insertar el usuario del certificado.");
		if (!verifyProperty("certPwd") && !verifyProperty("certPin"))
			throw new CertException(3, "Falta por insertar la clave o el pin del certificado.");
		if (!verifyProperty("pfx"))
			throw new CertException(3, "Falta por insertar el archivo PFX.");
		if (!verifyProperty("key"))
			throw new CertException(3, "Falta por insertar la clave 'key' del archivo PFX.");
		
		return !getProperties().containsKey("help");
	}

	public String getLogPropertiesFile() {
		return "configuration" + File.separator + LOG_PROPERTIES_FILE;
	}

	public String getClientPropertiesFile() {
		return "configuration" + File.separator + CLIENT_PROPERTIES_FILE;
	}

	public String generateOutputDir(String outputDir, String pattern) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
		String time = dateFormat.format(now);

		StringBuilder newOutputDir = new StringBuilder();
		if (outputDir.endsWith(File.separator)) {
			newOutputDir.append(outputDir).append(time);
		} else {
			newOutputDir.append(outputDir).append(File.separator).append(time);
		}

		File fPattern = new File(pattern);
		if (fPattern.exists()) {
			newOutputDir.append("_").append(fPattern.getName());
		} else {
			newOutputDir.append("_").append(pattern);
		}

		String finalOutputDir = newOutputDir.toString();
		File f = new File(finalOutputDir);
		int cursor = 1;
		while (f.exists()) {
			finalOutputDir = newOutputDir.toString() + "_" + cursor;
			f = new File(finalOutputDir);
			cursor++;
		}
		return finalOutputDir + File.separator;
	}

		public void generatedOutputFromContent(byte[] document, String filename, String extension, String finalOutputDir,
			String method, Object fromClass) throws IOException {
		File fFilename = new File(filename);
		String fileName = filename;
		if (fFilename.exists()) {
			fileName = fFilename.getName();
		}
		String newExtension = extension;
		if (StringUtils.isBlank(extension)) {
			newExtension = "";
		}
		if ((document != null) && (document.length > 0)) {
			org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(finalOutputDir + fileName + newExtension),
					document);
			GestorLogs.info(fromClass, method, "File generated successfully!");
		} else {
			GestorLogs.error(fromClass, method, CertException.getTextCode(5));
		}
	}

	public String getFilename(String[] filenames) {
		String filename;
		if (filenames[0].endsWith(PDF_EXTENSION)) {
			filename = filenames[1];
		} else {
			filename = filenames[0];
		}
		return filename;
	}

	abstract void addArguments(String[] paramArrayOfString) throws CertException;

	abstract void showHelp();

	abstract boolean validaDatos() throws CertException;
}
