/**
 * The Interface of
 *
 * @author Juan Colomer Martinez
 * @version 1.0
 */
package com.teknei.edicom.ecsv2;

import cert.ecs.edicom.com.GenerateUnrecognizedCertificate.CertParams;
import cert.ecs.edicom.com.GenerateUnrecognizedCertificate.CertParams.Entry;
import cert.ecs.edicom.com.WebServiceCert;
import org.apache.log4j.Logger;

import javax.xml.rpc.holders.StringHolder;
import java.util.Map;

public class EcsV2Client_cert extends EcsV2Client {

    private static final Logger LOG = Logger.getLogger(EcsV2Client_cert.class);

    private static final String ERROR_AUTH = "Error de autenticacion. ";


    /**
     * Gets the cert client.
     *
     * @param url     the url
     * @param pfxFile the pfx file
     * @param pfxPwd  the pfx pwd
     * @return the cert client
     * @throws Exception the exception
     */
    public static WebServiceCert getCertClient(String url, String pfxFile, String pfxPwd) throws Exception {
        return (WebServiceCert) getClientKeyStore(url, pfxFile, pfxPwd);
    }


    /**
     * Check status.
     *
     * @param url       the url
     * @param pfxFile   the pfx file
     * @param pfxPwd    the pfx pwd
     * @param checkType the check type
     * @return true, if successful
     */
    public static boolean checkStatus(String url, String pfxFile, String pfxPwd, int checkType) {
        try {
            WebServiceCert rddServer = getCertClient(url, pfxFile, pfxPwd);
            return rddServer.checkStatus(checkType);
            //
        } catch (Exception e) {
            LOG.error("", e);
            return false;
        }
    }


    /**
     * Generate unrecognized certificate.
     *
     * @param url             the url
     * @param pfxFile         the pfx file
     * @param pfxPwd          the pfx pwd
     * @param certUsr         the cert usr
     * @param certPwd         the cert pwd
     * @param restrictedLogin the restricted login
     * @param certParams      the cert params
     * @param error           the error
     * @return the byte[]
     */
    public static byte[] generateUnrecognizedCertificate(String url, String pfxFile, String pfxPwd, String certUsr, String certPwd, String restrictedLogin, Map<String, String> certParams, StringHolder error) throws Exception {
        byte[] result = null;
        error.value = "";
        //
        try {
            CertParams clientParams = new CertParams();
            WebServiceCert certServer = getCertClient(url, pfxFile, pfxPwd);
            for (Map.Entry<String, String> entry : certParams.entrySet()) {
                Entry cpe = new Entry();
                cpe.setKey(entry.getKey());
                cpe.setValue(entry.getValue());
                clientParams.getEntry().add(cpe);
            }
            return certServer.generateUnrecognizedCertificate(certUsr, certPwd, restrictedLogin, clientParams, null);
            //
        } catch (Exception e) {
            //modificacion
            throw e;
        }
        //
    }


    /**
     * Renew certificate.
     *
     * @param url        the url
     * @param pfxFile    the pfx file
     * @param pfxPwd     the pfx pwd
     * @param certSerial the cert serial
     * @param error      the error
     * @return the byte[]
     */
    public static byte[] renewCertificate(String url, String pfxFile, String pfxPwd, String certSerial, StringHolder error) {
        byte[] result = null;
        error.value = "";
        //
        try {
            WebServiceCert certServer = getCertClient(url, pfxFile, pfxPwd);
            return certServer.renewCertificate(certSerial);
            //
        } catch (Exception e) {
            if (e.getMessage().indexOf("404:") > -1) {
                error.value = ERROR_AUTH + e.getMessage();
            } else {
                error.value = e.getMessage();
            }
        }
        //
        return result;
    }


    /**
     * Revoke certificate.
     *
     * @param url        the url
     * @param pfxFile    the pfx file
     * @param pfxPwd     the pfx pwd
     * @param certSerial the cert serial
     * @param error      the error
     */
    public static void revokeCertificate(String url, String pfxFile, String pfxPwd, String certSerial, StringHolder error) {
        error.value = "";
        //
        try {
            WebServiceCert certServer = getCertClient(url, pfxFile, pfxPwd);
            certServer.revokeCertificate(certSerial);
            //
        } catch (Exception e) {
            if (e.getMessage().indexOf("404:") > -1) {
                error.value = ERROR_AUTH + e.getMessage();
            } else {
                error.value = e.getMessage();
            }
        }
    }


}
