package com.teknei.edicom.ecs.cert.helper;

import java.io.Serializable;
import org.apache.log4j.Logger;

public class GestorLogs
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final int LOG_TYPE_DEBUG = 1;
  public static final int LOG_TYPE_TRACE = 2;
  public static final int LOG_TYPE_INFO = 3;
  public static final int LOG_TYPE_WARN = 4;
  public static final int LOG_TYPE_ERROR = 5;
  public static final int LOG_TYPE_FATAL = 6;
  
  public static String getTextoLog(String accion, String texto)
  {
    return "[" + accion.toUpperCase() + "] " + texto;
  }
  
  public static void debug(Object object, String accion, String texto) {
    debug(object, accion, texto, null);
  }
  
  public static void trace(Object object, String accion, String texto) { trace(object, accion, texto, null); }
  
  public static void info(Object object, String accion, String texto) {
    info(object, accion, texto, null);
  }
  
  public static void warn(Object object, String accion, String texto) { warn(object, accion, texto, null); }
  
  public static void error(Object object, String accion, String texto) {
    error(object, accion, texto, null);
  }
  
  public static void fatal(Object object, String accion, String texto) { fatal(object, accion, texto, null); }
  
  public static void debug(Object object, String accion, String texto, Throwable e)
  {
    writeLog(object, accion, texto, 1, e);
  }
  
  public static void trace(Object object, String accion, String texto, Throwable e) { writeLog(object, accion, texto, 2, e); }
  
  public static void info(Object object, String accion, String texto, Throwable e) {
    writeLog(object, accion, texto, 3, e);
  }
  
  public static void warn(Object object, String accion, String texto, Throwable e) { writeLog(object, accion, texto, 4, e); }
  
  public static void error(Object object, String accion, String texto, Throwable e) {
    writeLog(object, accion, texto, 5, e);
  }
  
  public static void fatal(Object object, String accion, String texto, Throwable e) { writeLog(object, accion, texto, 6, e); }
  
  private static void writeLog(Object object, String accion, String texto, int type, Throwable e)
  {
    Logger log = Logger.getLogger(object.getClass());
    String text = getTextoLog(accion, texto);
    switch (type) {
    case 1: 
      if (log.isDebugEnabled()) {
        log.debug(text, e);
      }
      break;
    case 2: 
      if (log.isTraceEnabled()) {
        log.trace(text, e);
      }
      break;
    case 3: 
      log.info(text, e);
      break;
    case 4: 
      log.warn(text, e);
      break;
    case 5: 
      log.error(text, e);
      break;
    case 6: 
      log.fatal(text, e);
      break;
    }
  }
}
