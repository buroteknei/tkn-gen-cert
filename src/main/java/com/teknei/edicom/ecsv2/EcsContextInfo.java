package com.teknei.edicom.ecsv2;

public class EcsContextInfo {

    
    private String user;
    private String pass;
    private String keystoreLocation;            
    private String keystorePassword;
    private boolean test; //INCLUIR TEST
			

    //INCLUIR TEST
	public boolean isTest() {
		return test;
	}
	
	//INCLUIR TEST
	public void setTest(boolean test) {
		this.test = test;
	}


	public static EcsContextInfo getInstance(){		
		EcsContextInfo  ewCI = EcsContextHolder.getContextInfo();
		if (ewCI == null) {
			ewCI = new EcsContextInfo();
			EcsContextHolder.setContextInfo(ewCI);
		}
		return ewCI;
	}


    public String getUser () {
        return user;
    }
    
    public void setUser (String user) {
        this.user = user;
    }

    
    public String getPass () {
        return pass;
    }
    
    public void setPass (String pass) {
        this.pass = pass;
    }

    
    public String getKeystoreLocation () {
        return keystoreLocation;
    }
    
    public void setKeystoreLocation (String keystoreLocation) {
        this.keystoreLocation = keystoreLocation;
    }
    
    public String getKeystorePassword () {
        return keystorePassword;
    }
    
    public void setKeystorePassword (String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

	
	
}
