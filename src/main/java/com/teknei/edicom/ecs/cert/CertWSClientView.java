package com.teknei.edicom.ecs.cert;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.teknei.edicom.ecs.cert.client.CertException;


public class CertWSClientView
{
  protected static Logger logger = Logger.getLogger(CertWSClientView.class.getName());
  

  private String[] parameters;
  
  private String operation;
  

  public String[] getParameters()
  {
    return this.parameters;
  }
  
  public void setParameters(String[] parameters) {
    this.parameters = parameters;
  }
  
  public String getOperation() {
    return this.operation;
  }
  
  public void setOperation(String operation) {
    this.operation = operation;
  }
  
  void launchOperation(String[] args) throws CertException {
    if (getOperation().equalsIgnoreCase("GenCertificate")) {
      GenerateCertificate generateCertificate = new GenerateCertificate();
      generateCertificate.generateCertificateController(args);
    } else {
      showHelp();
    }
  }
  
  public void showHelp() {
    StringBuilder buffer = new StringBuilder();
    buffer.append("CertWSClientView \n");
    buffer.append("================= \n");
    buffer.append("How to use: CertWSClientView ");
    buffer.append("[Operation] (Parameters) \n\n");
    buffer.append("Where Operation must be one of the following: \n");
    buffer.append("\n");
    buffer.append("GENCERTIFICATE\n");
    buffer.append("\n");
    buffer.append("And Parameters must be enter by means of a configuration file, see configuration folder for details, \nor specifying -h for explanation: CertWSClientView [Operation] -h.");
    


    String help = buffer.toString();
    
    logger.setLevel(Level.INFO);
    logger.info(help);
  }
  
  boolean validaDatos() throws CertException {
    if (getParameters().length == 0) {
      return false;
    }
    return true;
  }
  
  public static void main(String[] args) throws CertException {
    CertWSClientView view = new CertWSClientView();
    view.setParameters(args);
    if (!view.validaDatos()) {
      view.showHelp();
    } else {
      view.setOperation(args[0]);
      String[] argsNew;
	if (args.length == 1) {
        argsNew = new String[0];
      } else {
        argsNew = (String[])Arrays.copyOfRange(args, 1, args.length);
      }
      view.launchOperation(argsNew);
    }
  }
}
