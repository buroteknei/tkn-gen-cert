package com.teknei.edicom.ecs.cert;

import com.teknei.edicom.ecs.cert.client.CertClient;
import com.teknei.edicom.ecs.cert.client.CertException;
import com.teknei.edicom.ecs.cert.helper.FileUtils;
import com.teknei.edicom.ecs.cert.helper.GestorLogs;

import cert.ecs.edicom.com.GenerateUnrecognizedCertificate.CertParams;

import com.sanityinc.jargs.CmdLineParser;
import com.sanityinc.jargs.CmdLineParser.Option;
import com.sanityinc.jargs.CmdLineParser.OptionException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class GenerateCertificate extends CertProgram {
	GenerateCertificate() throws CertException {

	}

	public void showHelp() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("GenCertificate: \n");
		buffer.append("\n");
		buffer.append("How to use: GenCertificate ");

		buffer.append("{-").append('u').append(", --").append("user").append("} user").append(", ");
		buffer.append("{-").append('p').append(", --").append("pass").append("} pass").append(", ");

		buffer.append("{-").append('u').append(", --").append("certUsr").append("} user").append(", ");
		buffer.append("{-").append('p').append(", --").append("certPwd").append("} pass").append(", ");

		GestorLogs.info(GenerateCertificate.class, "Help", buffer.toString());
	}

	void addArguments(String[] args) throws CertException {
		CmdLineParser parser = new CmdLineParser();

		Option<String> url = parser.addStringOption('l', "url");
		Option<String> pfx = parser.addStringOption('s', "pfx");
		Option<String> key = parser.addStringOption('k', "key");

		Option<String> user = parser.addStringOption('u', "user");
		Option<String> pass = parser.addStringOption('p', "pass");

		Option<String> certUsr = parser.addStringOption('u', "certUsr");
		Option<String> certPwd = parser.addStringOption('p', "certPwd");

		try {
			parser.parse(args, Locale.getDefault());
		} catch (OptionException optionException) {
			String texto = "Error while processing client parameters: ";
			GestorLogs.error(GenerateCertificate.class, "Add Arguments", texto + optionException.toString(), optionException);
			showHelp();
		}

		String urlValue = (String) parser.getOptionValue(url);
		insertaValorProperties("url", urlValue);

		String pfxValue = (String) parser.getOptionValue(pfx);
		insertaValorProperties("pfx", pfxValue);

		String keyValue = (String) parser.getOptionValue(key);
		insertaValorProperties("key", keyValue);

		String userValue = (String) parser.getOptionValue(user);
		insertaValorProperties("user", userValue);

		String passValue = (String) parser.getOptionValue(pass);
		insertaValorProperties("pass", passValue);

		String certUserValue = (String) parser.getOptionValue(certUsr);
		insertaValorProperties("certUsr", certUserValue);

		String certPwdValue = (String) parser.getOptionValue(certPwd);
		insertaValorProperties("certPwd", certPwdValue);
	}

	boolean validaDatos() throws CertException {
//		try {
//			String dirIn = getProperties().getProperty("in");
//			File f = new File(dirIn);
//			String file = getProperties().getProperty("file");
//			if (((f.exists()) && (f.list().length > 0)) || (StringUtils.isNotBlank(file))) {
//				validateIn = true;
//			}
//			String outDir = getProperties().getProperty("out");
//			f = new File(outDir);
//			if (f.exists()) {
//				validateOut = true;
//			}
//		} catch (Exception exception) {
//			GestorLogs.error(GenerateCertificate.class, "Exception", exception.getMessage(), exception);
//		}
//		return (validateIn) && (validateOut);
		return true;
	}

	protected byte[] generateUnrecognizedCertificate() throws CertException {
		byte[] result = null;
		GestorLogs.info(GenerateCertificate.class, "GENERATE CERTIFICATE", "start");

		try {
			byte[] pfxBytes = FileUtils.lecturaFicheroAsBytes(new File(getProperties().getProperty("pfx")));
			CertClient client = new CertClient(getProperties().getProperty("url"),
					getProperties().getProperty("user"), getProperties().getProperty("pass"),
					pfxBytes, getProperties().getProperty("key")
					);

			String certUsr = getProperties().getProperty("certUsr");
			String certPwd = getProperties().getProperty("certPwd");
			String certRestrictedLogin = getProperties().getProperty("certRestrictedLogin");
			
			CertParams certParams = new CertParams();
			CertParams.Entry entry = new CertParams.Entry();
			entry.setKey("C");entry.setValue("ES");
			certParams.getEntry().add(entry);
			entry = new CertParams.Entry();
			entry.setKey("O");entry.setValue("edicom");
			certParams.getEntry().add(entry);
			entry = new CertParams.Entry();
			entry.setKey("OU");entry.setValue("imasd");
			certParams.getEntry().add(entry);
			entry = new CertParams.Entry();
			entry.setKey("SN");entry.setValue("111222333");
			certParams.getEntry().add(entry);
			entry = new CertParams.Entry();
			entry.setKey("CN");entry.setValue("Juan Colomer");
			certParams.getEntry().add(entry);
			entry = new CertParams.Entry();
			entry.setKey("E");entry.setValue("jcolomer@edicom.es");
			certParams.getEntry().add(entry);
			
			List<byte[]> certEvidences = new ArrayList<byte[]>();
			certEvidences.add(new String("cid:173543174308").getBytes());
			
			result = client.generateUnrecognizedCertificate(certUsr, certPwd, certRestrictedLogin, certParams, certEvidences);
		} catch (Exception exception) {
			GestorLogs.error(GenerateCertificate.class, "Client Exception", "The client could not be connected", exception);
		}
		return result;
	}

	public void generateCertificateController(String[] args) throws CertException {
		try {
			if ((args.length != 0) && ((args[0].endsWith("help")) || (args[0].endsWith(String.valueOf('h'))))) {
				showHelp();
				return;
			}
			addArguments(args);
			GestorLogs.info(GenerateCertificate.class, "main", "Parametros: " + getProperties().toString());
			if ((verifyProperties()) && (validaDatos())) {
				byte[] generateUnrecognizedCertificate = generateUnrecognizedCertificate();
				generatedOutputFromContent((byte[])generateUnrecognizedCertificate, "salida", ".bin", "", "GenCertificate", GenerateCertificate.class);
			} else {
				GestorLogs.error(GenerateCertificate.class, "GenCertificate", CertException.getTextCode(5));
			}
			GestorLogs.info(GenerateCertificate.class, "main", "Terminated!");
		} catch (IOException ex) {
			GestorLogs.error(GenerateCertificate.class, "main", ex.getMessage(), ex);
		}
	}
}
