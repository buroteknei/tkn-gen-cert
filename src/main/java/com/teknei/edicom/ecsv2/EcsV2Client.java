package com.teknei.edicom.ecsv2;

import cert.ecs.edicom.com.WebServiceCert;
import cert.ecs.edicom.com.WebServiceCertImplService;
import org.apache.commons.io.FileUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.rpc.holders.StringHolder;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


public class EcsV2Client {

    protected static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EcsV2Client.class);


    protected static Map<String, EcsClient> hmClient = new HashMap<String, EcsClient>();

    protected static final long CLIENT_LAPSED = 1800000; //
    protected static final int TIMEOUT_CON = 60000;   // 1 min
    protected static final int TIMEOUT_REC = 300000;  // 5 min

    protected static int nReintentos = 0;

    private static final String MESSAGE_AUT_FAIL = "Los datos de autentificac";
    private static final String MESSAGE_CERT_ALREADY_EXISTS = "ya ha sido generado";
    private static final String MESSAGE_CERT_SERIAL_READ_PROBLEM = "El serial del certificado no pudo ser obtenido";
    private static final String ERROR_CODE_AUT_FAIL = "403";
    private static final String ERROR_CODE_CERT_ALREADY_EXISTSL = "409";
    private static final String ERROR_CODE_CERT_SERIAL_READ_PROBLEM = "400";


    protected static class EcsClient {
        long fcreacion;
        Object client;

    }


    public static Object getClientKeyStore(String url, String pfxFile, String pfxPwd) throws Exception {
        EcsClient ec = hmClient.get(url);
        if (ec == null) {
            // Si no hay cliente lo creamos
            ec = new EcsClient();
            ec.client = createClient(url);
            ec.fcreacion = new Date().getTime();
            hmClient.put(url, ec);
        } else if (ec.fcreacion + CLIENT_LAPSED < new Date().getTime()) {
            // Si hay cliente y ha caducado, lo renovamos
            ec.client = createClient(url);
            ec.fcreacion = new Date().getTime();
        }
        // Fijamos la autenticaci�n en el contexto para el interceptor
        EcsContextInfo eci = EcsContextInfo.getInstance();
        eci.setKeystoreLocation(pfxFile);
        eci.setKeystorePassword(pfxPwd);
        //
        return ec.client;
    }


    protected static Object createClient(String url) throws Exception {
        return getCertClient(url);
    }


    protected static WebServiceCert getCertClient(String url) throws Exception {
        try {
            URL wsdlLocation = new URL(url + "?wsdl");
            HttpsURLConnection.setDefaultHostnameVerifier(EcsClientUtils.hv);
            // Creamos el cliente
            WebServiceCertImplService service = new WebServiceCertImplService(wsdlLocation);
            WebServiceCert wscert = service.getWebServiceCertImplPort();
            //
            // Preparamos el interceptor de seguridad para autenticar con el certificado
            AbstractPhaseInterceptor<Message> wssOut = new ECSCertificateInterceptor();
            // Obtenemos el cliente
            Client client = org.apache.cxf.frontend.ClientProxy.getClient(wscert);
            HTTPConduit http = (HTTPConduit) client.getConduit();
            HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
            httpClientPolicy.setConnectionTimeout(TIMEOUT_CON);
            httpClientPolicy.setReceiveTimeout(TIMEOUT_REC);
            // proxy
            EcsClientUtils.setProxyParams(httpClientPolicy, http);
            //
            http.setClient(httpClientPolicy);
            http.setTlsClientParameters(EcsClientUtils.getTlsClientParameters());
            Endpoint cxfEndpoint = client.getEndpoint();
            cxfEndpoint.getOutInterceptors().add(wssOut);
            //
            return wscert;
            //
        } catch (Exception e) {
            LOG.error("getCertClient" + e);
            throw new Exception("No se ha podido conectar con el ecs <" + url + ">");
        }
    }

    private static String getSerialNumber(String keyStoreFile, String keyStorePassw, byte[] keyStoreBytes) {
        try {
            KeyStore kStore = KeyStore.getInstance("PKCS12");
            if (keyStoreBytes != null) {
                ByteArrayInputStream is = new ByteArrayInputStream(keyStoreBytes);
                kStore.load(is, keyStorePassw.toCharArray());
            } else {
                InputStream is = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(keyStoreFile)));
                try {
                    kStore.load(is, keyStorePassw.toCharArray());
                } finally {
                    is.close();
                }
            }
            Enumeration<String> e = kStore.aliases();
            String alias = (String) e.nextElement();
            //Key privateKey = kStore.getKey(alias, keyStorePassw.toCharArray());

            KeyStore.PrivateKeyEntry keyEntry
                    = (KeyStore.PrivateKeyEntry) kStore.getEntry(alias, new KeyStore.PasswordProtection(keyStorePassw.toCharArray()));

            X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

            System.out.println("cert name:" + cert.getSubjectX500Principal().getName());
            System.out.println("cert serial number: " + cert.getSerialNumber().toString(16));
            return cert.getSerialNumber().toString(16);

        } catch (NoSuchAlgorithmException e) {
            LOG.error("Error al inicializar el cliente", e);
            System.out.println("Error al inicializar el cliente " + e.getMessage());
        } catch (KeyStoreException e) {
            LOG.error("Error al inicializar el cliente", e);
            System.out.println("Error al inicializar el cliente " + e.getMessage());
        } catch (CertificateException e) {
            LOG.error("Error al inicializar el cliente", e);
            System.out.println("Error al inicializar el cliente " + e.getMessage());
        } catch (IOException e) {
            LOG.error("Error al inicializar el cliente", e);
            System.out.println("Error al inicializar el cliente " + e.getMessage());
        } catch (UnrecoverableKeyException e) {
            LOG.error("Error al inicializar el cliente", e);
            System.out.println("Error al inicializar el cliente " + e.getMessage());
        } catch (UnrecoverableEntryException e) {
            LOG.error("Error al inicializar el cliente", e);
            System.out.println("Error al inicializar el cliente " + e.getMessage());
        }
        return null;
    }

    /**
     * url - param0
     * keystorefile - param1
     * keypassword - param2
     * commonName - param3
     * id - param4
     * email - param5
     * certUsr - param6
     * certPwd - param7
     * outputcertdir - param8
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("---------------------------------JAVA_PROCESS---------------------------------------------");
        //String url = args[0];
        String url = "https://ecsweb.sedeb2b.com:9125/EdicomCryptoServer/services/cert";
        //String url = "https://212.49.145.110:9125/EdicomCryptoServer/services";
        //String url = "https://212.49.145.114:9125/EdicomCryptoServer/services/cert";
        //String keyStoreFile = args[1];// "/home/amaro/Documentos/TKN/PSC/BURO_SSL.P12"; //"/home/jcolomer/TREBALL/Certificados/test.p12";
        String keyStoreFile = "/home/amaro/Documentos/TKN/PSC/BURO_SSL.P12";
        //String keyStoreFile = "/home/amaro/Descargas/certificado_login_test.p12";
               // " //"/home/jcolomer/TREBALL/Certificados/test.p12";
        //String keyStoreFile = "C:\\00_Certificados\\BURO_SSL\\BURO_SSL.p12";// "/home/amaro/Documentos/TKN/PSC/BURO_SSL.P12"; //"/home/jcolomer/TREBALL/Certificados/test.p12";
        //String keyPassword = args[2];//"BID"; //"test";
        String keyPassword = "BID";//"BID"; //"test";
        //String keyPassword = "edicom";//"BID"; //"test";
        Map<String, String> certParams = new HashMap<String, String>();
        //certParams.put("SN", args[4]);//"11322999");
        certParams.put("SN", " Asuncion_6179440149UFT__bancsabadell_com_38");//"11322999");
        //String customerName = args[3];
        String customerName = "AOlaya_Exgea_Apellido_bastante_largo__bancsabadell_com_38";
        customerName = customerName.replace("_", " ");
        certParams.put("CN", customerName);//"Pe3ito JUAN XXXXX");
        //certParams.put("CN", "SNOPY PRUEBA");//"Pe3ito JUAN XXXXX");
        //certParams.put("E", args[5]);//"ppe3ezgarci@acme.es");
        certParams.put("E", "Olaya_6786782660UFT_mail_bastante_largo@bancsabadell.com");//"ppe3ezgarci@acme.es");
        StringHolder errorMessage = new StringHolder();
        //String certUsr = args[6];//"123456789C4"; //"BID170328LY0_SBDL";
        String certUsr = "BID170328LY0_SBDL"; //"BID170328LY0_SBDL";
        //String certPwd = args[7];//"plpn0wpcd9";
        String certPwd = "plpn0wpcd9";//"plpn0wpcd9";
        String certRestrictedLogin = "";
        String isTestText = "n";
        boolean isTest = false;
        if(isTestText.toUpperCase().contains("FALSE")){
            isTest = false;
        }
        //INCLUIR TEST - INICIO
        EcsContextInfo eci = EcsContextInfo.getInstance();
		eci.setTest(isTest);
		//INCLUIR TEST - FIN			
			
		byte[] result = new byte[0];
        String uriErrorFile = "/home/amaro/Descargas/_error.code";
        //String uriErrorFile = args[8] + "/" + args[4] + certUsr + "_error.code";
        try {
            result = EcsV2Client_cert.generateUnrecognizedCertificate(url, keyStoreFile, keyPassword, certUsr, certPwd, certRestrictedLogin, certParams, errorMessage);
        } catch (Exception e) {
            if (e.getMessage().toUpperCase().contains(MESSAGE_AUT_FAIL.toUpperCase())) {
                try {
                    Files.write(Paths.get(uriErrorFile), ERROR_CODE_AUT_FAIL.getBytes());
                    LOG.error("AUTHENTICATION FAILED");
                    System.out.println("Autenticacion fallida");
                } catch (IOException e1) {
                    LOG.error("No se pudo escribir en archivo de errores el codigo: " + ERROR_CODE_AUT_FAIL + " con mensaje de error: " + e1.getMessage());
                }
                LOG.info("Exit with status code: {}", 403);
                System.out.println("---------------------------------JAVA_PROCESS_END---------------------------------------------");
                System.exit(403);
            } else if (e.getMessage().toUpperCase().contains(MESSAGE_CERT_ALREADY_EXISTS.toUpperCase())) {
                try {
                    Files.write(Paths.get(uriErrorFile), ERROR_CODE_CERT_ALREADY_EXISTSL.getBytes());
                    LOG.error("CERT ALREADY CREATED");
                    System.out.println("Certificado ya creado");
                } catch (IOException e1) {
                    LOG.error("No se pudo escribir en archivo de errores el codigo: " + ERROR_CODE_CERT_ALREADY_EXISTSL + " con mensaje de error: " + e1.getMessage());
                }
                LOG.info("Exit with status code 409");
                System.out.println("---------------------------------JAVA_PROCESS_END---------------------------------------------");
                System.exit(409);
            }else{
                System.out.println("El mensaje no coincide con ningun codigo: "+e.getMessage());
                System.out.println(e.getMessage().toUpperCase().contains(MESSAGE_CERT_ALREADY_EXISTS.toUpperCase()));
            }
        }
        try {
            if (result != null) {
                //String uri = args[8] + "/" + args[4] + certUsr + ".p12";
                String uri = "/home/amaro/Descargas/" + certUsr + ".p12";
                System.out.println("CERT IN FORMAT P12 RETRIEVED");
                Files.write(Paths.get(uri), result);
            } else {
                LOG.error(errorMessage.value);
            }
            String serial = getSerialNumber(null, certPwd, result);
            //String uriSerial = args[8] + "/" + args[4] + certUsr + "_serial.txt";
            String uriSerial = "/home/amaro/Descargas/" + certUsr + "_serial.txt";
            LOG.info("Serial found: {}", serial);
            System.out.println("Serial: " + serial);
            if (serial == null) {
                System.out.println("Serial is null, exit with 400");
                try {
                    Files.write(Paths.get(uriErrorFile), ERROR_CODE_CERT_SERIAL_READ_PROBLEM.getBytes());
                    LOG.error(MESSAGE_CERT_SERIAL_READ_PROBLEM);
                } catch (IOException e1) {
                    LOG.error("No se pudo escribir en archivo de errores el codigo: " + ERROR_CODE_CERT_SERIAL_READ_PROBLEM + " con mensaje de error: " + e1.getMessage());
                    System.out.println("No se pudo escribir en archivo de errores el codigo: 400");
                }
                System.out.println("---------------------------------JAVA_PROCESS_END---------------------------------------------");
                System.exit(400);
            } else {
                Files.write(Paths.get(uriSerial), serial.getBytes());
            }
        } catch (Exception e) {
            LOG.error("No se pudo obtener el serial del certificado generado: " + e);
            System.out.println("No se pudo obtener el serial del certificado generado: " + e.getMessage());
            e.printStackTrace();
            try {
                Files.write(Paths.get(uriErrorFile), ERROR_CODE_CERT_SERIAL_READ_PROBLEM.getBytes());
                LOG.error(MESSAGE_CERT_SERIAL_READ_PROBLEM);
            } catch (IOException e1) {
                LOG.error("No se pudo escribir en archivo de errores el codigo: " + ERROR_CODE_CERT_SERIAL_READ_PROBLEM + " con mensaje de error: " + e1.getMessage());
                System.out.println("No se pudo escribir en archivo de errores el codigo: 400");
            }
            System.out.println("---------------------------------JAVA_PROCESS_END---------------------------------------------");
            System.exit(400);
        }
        try {
            Files.write(Paths.get(uriErrorFile), "0".getBytes());
            LOG.info("FIN DE PROCESO EXITOSO");
            System.out.println("Fin de proceso exitoso");
        } catch (IOException e1) {
            LOG.error("No se pudo escribir en archivo de errores el codigo: " + "0" + " con mensaje de error: " + e1.getMessage());
        }
        System.out.println("---------------------------------JAVA_PROCESS_END---------------------------------------------");
        System.exit(0);
    }
}
